# GroupeAkshay
# GROUPE Akshay

## Patel Akshay / Patel Akshay (un autre compte)

### TP GIT

#### ÉNONCÉ 

- Créer un projet groupeX sur Gitlab.com, y inviter les membres du groupe
- Ecrire le README.md selon la nomenclature en y précisant votre groupe, les membres et l'énoncé.
- Tirer une branche "develop" de la branche "main"
- Tirer une branche "feature<Nom-de-l-auditeur>" de la branche "develop" (ex: featureLebreton)
- Créer un fichier script-<nom-de-l-auditeur>.sh  (ex: script-lebreton.sh)

